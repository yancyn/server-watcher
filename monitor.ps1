# Below implementation is working in Powershell 5.1
# Replace your gmail username and password
# PS > powershell -ExecutionPolicy ByPass -File monitor.ps1

# TODO: Replace recipient email, your gmail username and password
$to = "";
$username = "";
$pass = "";

function SendMail($hostName)
{
    echo "Sending email...";
    $password = ConvertTo-SecureString $pass -AsPlainText -Force
    #$credential = Get-Credential;
    $credential = New-Object System.Management.Automation.PSCredential($username, $password);
    
    $params = @{
        From = "$username@gmail.com"
        To = $to
        Subject = "$hostName is offline"
        Body = "Please take appropriate action after receive this email."
        SMTPServer = "smtp.gmail.com"
        Port = 587
        UseSsl = $true
        Credential = $credential
    };
    Send-MailMessage @params;
    echo "Email sent";
}

# TODO: Start ping server alive if not send email
$servers = @("www.google.com", "192.168.0.160");
for($i=0;$i -lt $servers.Length; $i++) {
    $server = $servers[$i];
    echo "Ping $server";

    if (Test-Connection -ComputerName $server -Quiet) {
        # $wshell.Popup($server + " is up", 0, "Monitor", 0x0);
        # do nothing
        echo "$server is running";
    } else {
        echo "$server is down";
        SendMail $server;
    }
}
