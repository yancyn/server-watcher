# Server Watcher
Notify if a server unreachable.

## Pre-requisition
- NodeJs

## Get Started
1. Get gitter token and replace in function ``pushGitter()`` and also the chatroom id.
2. List out the host names you want to monitor in ``config.json``.
```
{
    "192.168.0.1": true,
    "192.168.0.2": true,
    "google.com": true
}
```

3. Install dependencies.
```
$ npm install node-gitter -save
$ npm install ping --save
```

## References
1. https://www.npmjs.com/package/net-ping
2. Example 7 in https://adamtheautomator.com/send-mailmessage/
3. https://adamtheautomator.com/send-mailmessage/
4. https://adamtheautomator.com/powershell-get-credential/