var fs = require("fs");
var Gitter = require("node-gitter"); // npm install node-gitter --save
var ping = require("ping"); // npm install ping --save

// get config value
var data = fs.readFileSync("./config.json", "utf8"),
  config;
try {
  config = JSON.parse(data);
  console.dir(config);
} catch (e) {
  console.log("Error:", e.stack);
}
//console.log("Token: " + config.gitter);

// config content to be updated
var body = "";
checkAlive().then(
  writeOutput());

async function checkAlive() {
  // jetson, win10
  var index = 0;
  for (key in config) {
    alive(key, index);
    index++;
  }
}


async function alive(host, index) {
  await pingHost(host, index);
}

/**
 * Ping a host by given name or IP address.
 * @param {string} host - Name or IP for a host.
 * @param {number} index - Index in config.
 */
function pingHost(host, index) {
  return new Promise(resolve => {
    setTimeout(() => {
      ping.sys.probe(host, function (isAlive) {
        var message = "host " + host + " is ";
        message += (isAlive) ? "alive" : "dead";
        console.log(message);

        if (index == 0) {
          body += "{";
        } else {
          body += ",\n";
        }

        if (isAlive) {
          body += '"' + host + '": true';
        } else {
          body += '"' + host + '": false';

          // only notify once when alive to dead.
          // Do not sending again if dead to dead
          if (config[host]) {
            pushGitter(message);
          }
        }
        return isAlive;
      });
    })
  });
}

/**
 * Send message to giter chatroom.
 * TODO: Set token
 * TODO: Get the room id from browser url NOT the friendly name you see
 * @param {string} message - Message send to chatroom.
 * @see https://www.npmjs.com/package/node-gitter
 */
function pushGitter(message) {

  // define gitter with sender token
  var token = "";
  var roomId = "";
  var gitter = new Gitter(token);
  gitter.rooms.join(roomId)
    .then(function (room) {
      room.send(message);
      console.log("Message sent");
    });
}

async function writeOutput() {
  await printConfig();
}

/**
 * Update server states in config.json.
 */
function printConfig() {
  return new Promise(resolve => {
    setTimeout(() => {
      body += "}"; // HACK: Fail to append outside of promise.
      fs.writeFile("config.json", body, function (err) {
        if (err) {
          throw err;
        }
        console.log("Config updated successfully!");
      });
    }, 5000)
  });
}

